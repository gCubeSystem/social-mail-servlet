
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.0.0] - 2024-04-18

- Removed social networking lib dep

## [v2.5.0] - 2021-06-22

- Feature #21689 Social Mail Servlet to StorageHub migration

- Removed HomeLibrary Dependency

## [v2.4.0] - 2019-12-30

- Ported to git

## [v2.3.0] - 2018-08-30

- Fix for Incident #12351 Email Parser stuck on AGINFRA Gateway

- Support #12332 Notifications via emails not working for bfr.bund.de domain

- Feature #12613, Replace use of "$" character with "_" when sending notification emails about posts and messages

## [v2.1.1] - 2017-02-03

- Minor refactor for comment replies

- Resilient to Email server failures

## [v2.1.0] - 2016-08-31

-  Added possibility to change poller time at runtime as well as to quit it by editing a property file in the server

-  Added support for oAuth2

## [v1.2.0] - 2015-10-14

- Added support for reply via email in Messages

- Read LDAP service endpoint from infrastructure

- Fix for bug #577

## [v1.0.0] - 2015-07-07

- First release
